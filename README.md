### Bot Monitor (backend) ###

 The System for monitoring and controlling many bots for Path of Exile based on analysis of bots log (exilebuddy bot).
 
### Futures
 
1. Monitoring the current state of characters (gold, level, location)
2. Monitoring and controlling the current state of botting (enable/disable/updating, state of controlling (farming/ level up), keys)
3. Monitoring the state of the game (enable/disable/updating)


### Instruments ###


 1. RabbitMQ 
2. ElastichSearch+Kibana for analysis of the game characters 
3. analysis of game logs 
##

### Bot Agent (service)

https://bitbucket.org/MetaRabbit/poebotagent

### Front-end

https://bitbucket.org/MetaRabbit/botmonitormvcclient

### Identity Server based on IdentityServer3

https://bitbucket.org/MetaRabbit/identityserver

### Service Registry

https://bitbucket.org/MetaRabbit/serviceregistry

### Project Status ###

Released