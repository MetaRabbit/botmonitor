﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using Npgsql;

namespace BotMonitor.Infrastruction
{
    public class Repository
    {
        public static string ConnectionString =
            @"Host=localhost;Database=PersonalDb;Username=postgres;Password=Rola1994";

        protected IDbConnection db;

        protected Repository()
        {

        }
        protected IDbConnection GetOpenConnection()
        {
            db = new NpgsqlConnection(ConnectionString);
          
            db.Open();
            return db;
        }
    }
}
