﻿using System;

namespace BotMonitor.Infrastruction.Utils
{
    public static class DateConvert
    {

        public static int ToUnixTimestamp(this DateTime value)
        {
            return (int)Math.Truncate((value.ToUniversalTime().Subtract(new DateTime(1970, 1, 1))).TotalSeconds);
        }
    }
}
