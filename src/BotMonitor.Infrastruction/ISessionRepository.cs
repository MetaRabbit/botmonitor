﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Data;

namespace BotMonitor.Infrastruction
{
    public interface ISessionRepository
    {
        Task Add(String botname, Session botAgentInfo);
        Task Update(String botname, Session botAgentInfo);
        List<Session> GetBotAgentInfoByServer(string serverName, string botName);
    }
}
