﻿using System;
using System.IO;
using System.Threading.Tasks;
using Elasticsearch.Net;
using Microsoft.Extensions.Logging;
using Nest;

namespace BotMonitor.Infrastruction
{

    public class GameDataRepository
    {

        private ElasticLowLevelClient _client;

        private string _simpleSearchQuery = @"{  
            
                                       
                          ""filter"": {
                                 ""match_all"":  { } 
                        }                                                   
                     ,
                  ""sort"": [
                    {
                      ""TimeStamp"": {
                        ""order"": ""desc""
                      }
                    }
                  ],
                  ""size"": 1
               } ";


        public GameDataRepository()
        {
            var node = new Uri("http://localhost:9200");

            var settings = new ConnectionSettings(node);
            _client = new ElasticLowLevelClient(settings);
        }


        public async Task AddExileBuddyData(string botName, string id, string jsonData)
        {
            await _client.IndexAsync<string>(botName, id, jsonData);
        }

        public async Task<string> GetExileBuddyDataAsync(string botName, int idBotAgent)
        {
            var indexInfo = await _client.IndicesExistsAsync<string>(botName);
            if (!indexInfo.Success)
            {
                throw new IOException("Error ES connection.");
            }
            if (indexInfo.HttpStatusCode == 404)
            {
                throw new ArgumentException("Index ES not found: "+ botName);
            }
            var typeInfo = await _client.IndicesExistsTypeAsync<string>(botName, idBotAgent.ToString());
            if (typeInfo.HttpStatusCode == 404)
            {
                throw new ArgumentException("Type ES not found: "+ idBotAgent);
            }

            var lastInfo = await _client.SearchAsync<string>(botName, idBotAgent.ToString(), _simpleSearchQuery);

            return EsHelper.ConvertHitToJsonInstanceSource(lastInfo.Body) ;
        }

        
    }
}
