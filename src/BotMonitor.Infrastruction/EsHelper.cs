﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using BotMonitor.Infrastruction.Utils;
using Newtonsoft.Json.Linq;

namespace BotMonitor.Infrastruction
{
    public class EsHelper
    {
        public static string AddTimeStamp(string jsonData)
        {
            JObject jObject = JObject.Parse(jsonData);
            jObject.Add("@timestamp", $"{DateTime.UtcNow.ToLocalTime().Year}-{DateTime.UtcNow.ToLocalTime().Month}-{DateTime.UtcNow.ToLocalTime().Day}T{DateTime.UtcNow.ToLocalTime().Hour}:{DateTime.UtcNow.ToLocalTime().Minute}:{DateTime.UtcNow.ToLocalTime().Second}:{DateTime.UtcNow.ToLocalTime().Millisecond}Z");

            jObject.Add("TimeStamp", DateTime.UtcNow.ToLocalTime().ToUnixTimestamp());
            return jObject.ToString();
        }
        public static string ConvertHitToJsonInstanceSource(string json)
        {
            JObject jObject = JObject.Parse(json);
            var res = jObject["hits"]["hits"].Select(obj => obj["_source"].ToString()).FirstOrDefault();
            return res;

        }

    }
}
