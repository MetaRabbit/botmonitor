﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Data
{
    public class BotCommand
    {
        public BotCommand()
        {
            Services = new List<Service>();
        }


        public string Name { get; set; }
        public List<Service> Services { get; set; }
    }
}
