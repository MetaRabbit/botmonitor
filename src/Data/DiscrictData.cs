﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Data
{
    public class DiscrictData
    {
        public string Name { get; set; }
        public int Total { get; set; }
        public int NActive { get; set; }
        public int Nban { get; set; }
        public int NKeyExipred { get; set; }
        public int NManualControl { get; set; }
        public bool Available { get; set; }

    }
}
