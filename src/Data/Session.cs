﻿using System.Collections.Generic;

namespace Data
{
    public class Session 
    {
        public Session()
        {
            Services = new List<BotAgentService>();
            BAgentState = "";
        }

        public int Id { get; set; }
        public string BotState { get; set; }
        public string GameState { get; set; }
        public string BAgentState { get; set; }
        public string LogState { get; set; }
        public string District { get; set; }
        public bool ManualControl { get; set; }
        public bool IsNeedUpdate { get; set; }
        public long StartTime { get; set; }
        public long TimeLeft { get; set; }
        public long LastActivity { get; set; }
        public bool Registered { get; set; }
        public List<BotAgentService> Services; 

        public void CopyTo(Session stateInfo)
        {
            stateInfo.Id = Id;
            stateInfo.BotState = BotState;
            stateInfo.District = District;
            stateInfo.GameState = GameState;
            stateInfo.LogState = LogState;
            stateInfo.ManualControl = ManualControl;
            stateInfo.BAgentState = BAgentState;

            stateInfo.IsNeedUpdate = IsNeedUpdate;
            stateInfo.LastActivity = LastActivity;
            //stateInfo.StartTime = StartTime;
            stateInfo.TimeLeft = TimeLeft;

            stateInfo.Services = Services;
            //stateInfo.Registered = Registered;
        }
    }
}
