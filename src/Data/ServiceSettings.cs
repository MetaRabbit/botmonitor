﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Data
{
    public class ServiceSettings
    {
        public ServiceSettings()
        {
            AdvanceItems = new List<string>();
        }

        public bool? AutoStart { get; set; }
        public List<string> AdvanceItems { get; set; }
    }
}
