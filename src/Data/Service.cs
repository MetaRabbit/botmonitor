﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Data
{
    public class Service
    {
        public string Name { get; set; }
        public bool Enable { get; set; }
        public int TotalItems { get; set; }
        public int FreeItems { get; set; }
        public int BadItems { get; set; }
    }
}
