﻿namespace Data
{
    public class GameInfo
    {
        public int TypeBot { get; set; }
        public string Data { get; set; }
        public int IdBotAgent { get; set; }
        public string GameUnavailable { get; set; }
    }
}
