﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Data
{
    public class BotAgentService
    {

        public BotAgentService()
        {
            Settings = new ServiceSettings();
        }

        public string Name { get; set; }
        public bool Running { get; set; }
        public bool Ready { get; set; }
        public ServiceSettings Settings { get; set; }

    }
}
