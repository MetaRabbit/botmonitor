﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using BotMonitor.Service.Commands;

namespace BotMonitor.Service
{
    public class CommandPublisherService
    {
        public async Task Publish(int typeBot,int botAgentId, List<IServiceCommand> commands)
        {
            foreach (var serviceCommand in commands)
            {
                using (var httpClient = new HttpClient())
                {
                    httpClient.BaseAddress = new Uri("http://localhost:5000");
                    var stringContent = new StringContent(serviceCommand.CalculateBody, Encoding.UTF8, "application/json");
                    var result =  httpClient.PostAsync(serviceCommand.URL, stringContent).Result;
                }
            }
        }

        public void Publish(int typeBot, int botAgentId, IServiceCommand command)
        {
            //ТУТ КОСТЫЛЬ. TODO Отправка в очередь
            using (var httpClient = new HttpClient())
            {
                httpClient.BaseAddress = new Uri("http://localhost:5000");
                var stringContent = new StringContent(command.CalculateBody, Encoding.UTF8, "application/json");
                httpClient.PostAsync("/bostate", stringContent);
            }
        }
    }
}
