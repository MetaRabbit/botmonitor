﻿using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.DotNet.InternalAbstractions;

namespace BotMonitor.Service
{
    public class UpdateBotAgentService
    {
        private IHostingEnvironment _hostingEnvironment;

        public UpdateBotAgentService(IHostingEnvironment hostingEnvironment)
        {
            _hostingEnvironment = hostingEnvironment;
        }

        public byte[] GetLastArchive()
        {
            if (!ArchiveIsExists() || !VersionFileIsExists())
                return null;
            return
                File.ReadAllBytes(Path.Combine(_hostingEnvironment.ContentRootPath,"Resource",
                    "botagent.zip"));
        }

        public bool ArchiveIsExists()
        {
            return File.Exists(Path.Combine(_hostingEnvironment.ContentRootPath,"Resource", "botagent.zip"));
        }

        public bool VersionFileIsExists()
        {
            return File.Exists(Path.Combine(_hostingEnvironment.ContentRootPath,"Resource", "version.txt"));
        }

        public string[] GetHashLastArchive()
        {
            return !VersionFileIsExists() ? null : File.ReadAllLines(Path.Combine(_hostingEnvironment.ContentRootPath, "Resource", "version.txt"));
        }
    }
}
