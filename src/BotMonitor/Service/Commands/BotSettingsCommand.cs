﻿using System.IO;
using Newtonsoft.Json;

namespace BotMonitor.Service.Commands
{
    public class BotSettingsCommand : IServiceCommand
    {

        public BotSettingsCommand(string nameSettings,string nameMode, string nameBot, string content)
        {
            Name = nameMode;
            NameBot = nameBot;
            NameSettings = nameSettings;
            Content = content;
        }

        public string CalculateBody { get; }
        public string Name { get; private set; }
        public string NameBot { get; private set; }
        public string NameSettings { get; private set; }
        public string Content { get; private set; }

        public string URL => "api/botsettings/" + NameSettings;
    }
}
