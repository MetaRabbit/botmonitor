﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace BotMonitor.Service.Commands
{
    public class BotStateCommand : IServiceCommand
    {
        public BotStateCommand()
        {
           
        }

        public string Args { get; set; }
        public bool? IsNeedUpdate { get; set; }
        public bool? MControl { get; set; }


        public string CalculateBody
        {
            get
            {             
                JObject jObject = new JObject();
                if (!string.IsNullOrEmpty(Args))
                {
                    jObject.Add("StartArgs", Args);
                }

                if (IsNeedUpdate != null)
                {
                    jObject.Add("IsNeedUpdate", IsNeedUpdate);
                }

                if (MControl != null)
                {
                    jObject.Add("ManualControl", MControl);
                }

                return jObject.ToString();
            } 
        }

        public string URL  => "api/BotState";
    }
}
