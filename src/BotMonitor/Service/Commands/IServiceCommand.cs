﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BotMonitor.Service.Commands
{
    public interface IServiceCommand
    {
        string CalculateBody { get; }
        string URL { get; }
    }
}
