﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BotMonitor.Service.Commands;

namespace BotMonitor.Service
{
    public interface IGameService
    {
        Task<List<IServiceCommand>> Calculate(string jsonData);
        bool Validate(string jsonData);
    }
}
