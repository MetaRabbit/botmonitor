﻿using System;
using System.Threading;
using BotMonitor.Enum;
using BotMonitor.Repository;
using BotMonitor.Utils;

namespace BotMonitor.Service
{
    public class BotAgentTurnOffNotifyService : IDisposable
    {
        private Timer _timer;
        private SessionRepository _sessionRepository;
        private DistrictRepository _districtRepository;
        private long _maxIdleSecond = 300;

        public BotAgentTurnOffNotifyService(SessionRepository sessionRepository, DistrictRepository districtRepository)
        {
            _sessionRepository = sessionRepository;
            _districtRepository = districtRepository;
        }

        public void Start()
        {

            _timer = new Timer(state =>
            {

                foreach (var s in System.Enum.GetNames(typeof(Bots)))
                {
                    var districs = _districtRepository.GetDistrictsByBotName(s);

                    foreach (var distric in districs)
                    {
                        var sessions = _sessionRepository.GetSessions(s, distric);
                        foreach (var session in sessions)
                        {

                            session.BAgentState = DateTime.UtcNow.Add(TimeSpan.FromHours(3)).ToUnixTimestamp() - session.LastActivity > _maxIdleSecond ? "DRun" : "Run";
                        }
                    }
                    
                }


            },null,0, 60000);
        }

        public void Dispose()
        {
            _timer?.Dispose();
        }
    }
}
