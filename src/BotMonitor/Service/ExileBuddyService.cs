﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using BotMonitor.Infrastruction;
using BotMonitor.Service.Commands;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace BotMonitor.Service
{
    public class ExileBuddyService : IGameService
    {
        GameDataRepository _gameDataRepository;
        List<Quest> Quests;
        private ILogger<ExileBuddyService> _logger;


        public ExileBuddyService(GameDataRepository gameDataRepository, ILogger<ExileBuddyService> logger)
        {
            _gameDataRepository = gameDataRepository;
            _logger = logger;
         

        }

        public async Task<List<IServiceCommand>> Calculate(string jsonData)
        {

            List<IServiceCommand> commands = new List<IServiceCommand>();

            JObject jObject = JObject.Parse(jsonData);

            List<Quest> activeQuests = new List<Quest>();

            foreach (var quest in Quests)
            {
                foreach (var VARIABLE in jObject["CurrentQuests"].ToObject<List<string>>())
                {
                    if (VARIABLE.Equals(quest.QuestName) && jObject["CurrentDifficulty"].ToString().Equals(quest.Difficulty))
                    {
                        activeQuests.Add(quest);
                    }
                }
               // activeQuests.AddRange(from questName in jObject["CurrentQuests"].ToObject<List<string>>() where quest.QuestName.Equals(questName) select quest);
            }
            //Проверка существование активного квеста из контрольных
            if (activeQuests.Any())
            {
                if (!jObject["CurrentBot"].ToString().Equals("OldGrindBot") &&
             activeQuests.Any(quest => quest.ReqLevel >= jObject["CurrentLevel"].ToObject<int>()))
                {
                    _logger.LogInformation($"{DateTime.UtcNow.ToLocalTime()} : Switch To Leveling");
                    //Переключение на leveling                 
                    commands = GetCommandsForMods("Leveling");

                }
                if (!jObject["CurrentBot"].ToString().Equals("QuestBot") && activeQuests.Any(quest => quest.ReqLevel < jObject["CurrentLevel"].ToObject<int>()))
                {
                    _logger.LogInformation($"{DateTime.UtcNow.ToLocalTime()} : Switch To Questing");
                    //Переключение на квестинг
                    commands = GetCommandsForMods("Quest");
                    
                }
            }

            return commands;
            //Если не гриндбот и есть квест требуемый уровень которого ниже уровня персонажа то переключить на режим левелинга
            //Если не квестбот и есть квестуровенькоторого >= требуемому то режим квестинга
            //Если уровень > 70 и сложность Merciless,Act: 4 то перейти в режим хайенда и больше никаких переключений режима

        }

        public List<IServiceCommand> GetCommandsForMods(string nameMode)
        {

            var path = Path.GetFullPath(Path.Combine("Resource","ExileBuddy", "Modes", nameMode));
            var files = Directory.GetFiles(path);
            List<IServiceCommand> serviceCommands = new List<IServiceCommand>();
            foreach (var file in files)
            {
              //  serviceCommands.Add(new BotSettingsCommand(Path.GetFileNameWithoutExtension(file), nameMode, "ExileBuddy", ));
            }

            return serviceCommands;
        }


        public bool Validate(string jsonData)
        {
            JObject jObject = JObject.Parse(jsonData);

            foreach (var child in jObject.Children())
            {
                try
                {
                    var obj = child.ToObject<string>();
                    if (obj.Equals("None"))
                    {
                        return false;
                    }

                }
                catch (Exception)
                {


                }
            }
            return true;
        }
    }


    public class Quest
    {

        public string QuestName;
        public string StartMapName;
        public int Act;
        public string Difficulty;
        public int ReqLevel;

    }
}
