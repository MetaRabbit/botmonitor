﻿using System;
using System.Collections.Generic;
using System.Linq;
using BotMonitor.Enum;
using BotMonitor.Utils;
using Data;

namespace BotMonitor.Repository
{
    public class SessionRepository
    {
        //Для ExileBuddy
        static Dictionary<string,Dictionary<string, List<Session>>> _botAgentInfos;
        static object _object = new object();
        DistrictRepository _districtRepository;

        public SessionRepository(DistrictRepository districtRepository)
        {
            _districtRepository = districtRepository;
            _botAgentInfos = new Dictionary<string, Dictionary<string, List<Session>>>();
            //Инициализация для миров
            Load();
            
        }

        public void AddOrUpdateSession(String botname, string distName, Session botAgentInfo)
        {
            if (!ExistsBot(botname) || !_districtRepository.ExistsBotDistricts(botname, distName))
            {
                return;
            }

            lock (_object)
            {
                var existBotAgentInfo = GetSession(botname, distName, botAgentInfo.Id);
                if (existBotAgentInfo != null)
                {
                    botAgentInfo.TimeLeft = DateTime.UtcNow.Add(TimeSpan.FromHours(3)).ToUnixTimestamp() - existBotAgentInfo.StartTime;
                    botAgentInfo.LastActivity = DateTime.UtcNow.Add(TimeSpan.FromHours(3)).ToUnixTimestamp();
                    botAgentInfo.BAgentState = "Run";
                    botAgentInfo.CopyTo(existBotAgentInfo);

                }                          
                else
                {
                    botAgentInfo.StartTime = DateTime.UtcNow.Add(TimeSpan.FromHours(3)).ToUnixTimestamp();
                    botAgentInfo.LastActivity = DateTime.UtcNow.Add(TimeSpan.FromHours(3)).ToUnixTimestamp();
                    _botAgentInfos[botname][distName].Add(botAgentInfo);
                }
            }

        }

        public Session GetSession(String botname, string distName, int id)
        {
            if (string.IsNullOrEmpty(botname) || string.IsNullOrEmpty(distName))
            {
                return null;
            }
            return _botAgentInfos[botname][distName].FirstOrDefault(info => info.Id.Equals(id));
        }

        public static bool ExistsBot(string name)
        {
            return !string.IsNullOrEmpty(name) || _botAgentInfos.ContainsKey(name);
        }
     
        void Load()
        {
            lock (_object)
            {
                foreach (var s in System.Enum.GetNames(typeof(Bots)))
                {

                    _botAgentInfos.Add(s, new Dictionary<string, List<Session>>());
                    if (_districtRepository.GetDistrictsByBotName(s) == null)
                    {
                        continue;
                    }
                    foreach (var distName in _districtRepository.GetDistrictsByBotName(s))
                    {
                        _botAgentInfos[s].Add(distName, new List<Session>());
                    }
                }
            }        
        }

        public List<Session> GetSessions(string botName, string district)
        {
            if (!ExistsBot(botName))
            {
                return null;
            }
            return _botAgentInfos[botName][district];
        }

        public List<DiscrictData> GetDistricts(string botName)
        {
            if (!ExistsBot(botName))
            {
                return new List<DiscrictData>();
            }

            var districts = _districtRepository.GetDistrictsByBotName(botName);
            List<DiscrictData> discricts = new List<DiscrictData>();

            foreach (var district in districts)
            {
                List<Session> stateInfos = GetSessions(botName, district);

                discricts.Add(new DiscrictData()
                {
                    Name = district,
                    Total = stateInfos.Count,
                    NActive = stateInfos.Count(info => info.LogState.Equals("Run")),
                    NKeyExipred = stateInfos.Count(info => info.LogState.Equals("KeyExpired")),
                    NManualControl = stateInfos.Count(info => info.ManualControl),
                    Nban = stateInfos.Count(info => info.LogState.Equals("Banned")),
                    Available = !stateInfos.All(info => info.GameState.Equals("Unavalaible"))
                   
            });
            }

            return discricts;
        }

        //public BotingGeneralStateInfo GetBotingGeneralStateInfo(string botName)
        //{
        //    if (!ExistsBot(botName))
        //    {
        //        return new BotingGeneralStateInfo();
        //    }
          
        //    var districts = _districtRepository.GetDistrictsByBotName(botName);
        //    if (districts == null)
        //    {
        //        return new BotingGeneralStateInfo();
        //    }
        //    var disctrictStateInfos = districts.Select(district => GetBotingDisctrictStateInfo(botName, district)).ToList();
        //    BotingGeneralStateInfo stateInfo = new BotingGeneralStateInfo
        //    {
        //        BotName = botName,
        //        NActive = disctrictStateInfos.Sum(info => info.NActive),
        //        NDistrictUnavailable = disctrictStateInfos.Count(info => !info.Available),
        //        NKeyExipred = disctrictStateInfos.Sum(info => info.NKeyExipred),
        //        NManualControl = disctrictStateInfos.Sum(info => info.NManualControl),
        //        Nban = disctrictStateInfos.Sum(info => info.Nban)
        //    };
        //    //TODO ЕСЛИ КОЛ-ВО ЗАБАННЫХ В ДЕНЬ СЛИШКОМ БОЛЬШОЕ
        //    stateInfo.IsBanWave = stateInfo.Nban > 6;
        //    return stateInfo;
        //}


    }
}
