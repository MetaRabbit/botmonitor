﻿using System.Collections.Generic;
using BotMonitor.Enum;
using BotMonitor.Service.DistrictService;
using Data;

namespace BotMonitor.Repository
{
    public class DistrictServiceRepository
    {
        private DistrictRepository _districtRepository;
        Dictionary<string, Dictionary<string, List<Data.Service>>> _districtServices;
        static object _object = new object();

        public DistrictServiceRepository(DistrictRepository districtRepository)
        {
            _districtRepository = districtRepository;
            _districtServices = new Dictionary<string, Dictionary<string, List<Data.Service>>>();

            lock (_object)
            {
                foreach (var s in System.Enum.GetNames(typeof(Bots)))
                {
                    _districtServices.Add(s, new Dictionary<string, List<Data.Service>>());
                    var districts = _districtRepository.GetDistrictsByBotName(s);
                    List<DiscrictData> discricts = new List<DiscrictData>();

                    foreach (var district in districts)
                    {
                        _districtServices[s].Add(district, new List<Data.Service>());
                        //Добавление всех стандартных сервисов
                        _districtServices[s][district].Add(new AccountService());
                    }                  
                }
            }
        }

        public List<Data.Service> GetServices(string botName, string districtName)
        {
            if (!SessionRepository.ExistsBot(botName))
            {
                return new List<Data.Service>();
            }

            return _districtServices[botName][districtName];
        }



    }
}
