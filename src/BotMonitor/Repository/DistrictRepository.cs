﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using BotMonitor.Enum;
using Newtonsoft.Json;

namespace BotMonitor.Repository
{
    public class DistrictRepository
    {
        Dictionary<string, List<string>> Districts;

        public DistrictRepository()
        {
            Districts = new Dictionary<string, List<string>>();

        }

        public bool ExistsBotDistricts(string botname, string district)
        {
            Load();
            if (Districts.ContainsKey(botname))
            {
                return Districts[botname].FirstOrDefault(s => s.Equals(district)) != null;
            }
            return false;
        }

        public List<string> GetDistrictsByBotName(string name)
        {
            Load();
            return Districts.ContainsKey(name) ? Districts[name] : null;
        }

        void Load()
        {
            foreach (var s in System.Enum.GetNames(typeof(Bots)))
            {
                string path = Path.GetFullPath(Path.Combine("Resource", s, @"Districts\district.json"));
                if (File.Exists(path))
                {
                    Districts[s] = JsonConvert.DeserializeObject<List<string>>(File.ReadAllText(path));
                }
            }
        }
    }
}
