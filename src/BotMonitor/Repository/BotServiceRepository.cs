﻿using System.Collections.Generic;
using BotMonitor.Enum;
using BotMonitor.Service;
using Data;

namespace BotMonitor.Repository
{
    public class BotServiceRepository
    {
        Dictionary<string, List<Data.Service>> _botServices;
        static object _object = new object();

        public BotServiceRepository()
        {
            _botServices = new Dictionary<string, List<Data.Service>>();

            lock (_object)
            {
                foreach (var s in System.Enum.GetNames(typeof(Bots)))
                {
                    //Добавление необходимых сервисов
                    _botServices[s] = new List<Data.Service>
                    {
                        new KeysService()
                    };

                }
            }

        }

        public List<Data.Service> GetServices(string botName)
        {
            if (!SessionRepository.ExistsBot(botName))
            {
                return new List<Data.Service>();
            }

            return _botServices[botName];
        } 

    }
}
