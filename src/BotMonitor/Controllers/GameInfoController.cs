﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BotMonitor.Extends;
using BotMonitor.Infrastruction;
using BotMonitor.Service;
using Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace BotMonitor.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    public class GameInfoController : ControllerBase
    {
        ExileBuddyService _exileBuddyService;
        GameDataRepository _gameDataRepository;
        public static string BaseAddress = "http://localhost:1941";
        CommandPublisherService _commandPublisherService;
        private ILogger<GameInfoController> _logger;

        public GameInfoController(ExileBuddyService exileBuddyService, GameDataRepository gameDataRepository, CommandPublisherService commandPublisherService, ILogger<GameInfoController> logger)
        {
            _exileBuddyService = exileBuddyService;
            _gameDataRepository = gameDataRepository;
            _commandPublisherService = commandPublisherService;
            _logger = logger;
        }

        // GET: api/values
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public async Task<string> Get(string botname,int id)
        {
            try
            {
                string data = await _gameDataRepository.GetExileBuddyDataAsync(botname, id);
                if (data == null)
                {
                    Response.NotFoundRequest();
                }
                return data;
            }
            catch (Exception e)
            {
                Response.ServerErrorRequest();
                _logger.LogError(e.Message);
                return "";
            }
         
            
        }

        // POST api/values
        [HttpPost]

        public async Task<ActionResult> Post([FromBody] GameInfo value)
        {
            if (value == null)
            {
                return new BadRequestResult();
            }


            if (!HttpContext.ConstrainsScope("gameinfoapi")) return new UnauthorizedResult();


            switch (value.TypeBot)
                {
                    case 0:
                    {
                            //Проверка типа бота
                            if (_exileBuddyService.Validate(value.Data))
                            {
                                
                                //await _commandPublisherService.Publish(value.TypeBot, value.IdBotAgent, await _exileBuddyService.Calculate(value.Data));
                                //Убираем для отладки
                                await _gameDataRepository.AddExileBuddyData("exilebuddy",value.IdBotAgent.ToString(), EsHelper.AddTimeStamp(value.Data));
                                return new OkResult();
                            }
                            else
                            {
                                return new StatusCodeResult(Response.StatusCode);
                            }

                    }
                }

           // }

            return new UnauthorizedResult();

        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
