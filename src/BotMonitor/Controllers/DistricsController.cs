﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BotMonitor.Repository;
using BotMonitor.Service;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace BotMonitor.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    public class DistricsController : ControllerBase
    {
        SessionRepository _sessionRepository;

        public DistricsController(SessionRepository sessionRepository)
        {
            _sessionRepository = sessionRepository;
        }

        // GET api/values/5
        [HttpGet]
        public ActionResult Get(string botname)
        {
            // if (!HttpContext.ConstrainsScope("botagentreadapi")) return new UnauthorizedResult();
            return new JsonResult(_sessionRepository.GetDistricts(botname));
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
