﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BotMonitor.Models;
using BotMonitor.Repository;
using BotMonitor.Service;
using BotMonitor.Service.Commands;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace BotMonitor.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    public class BotAgentCommandController : Controller
    {
        private CommandPublisherService _commandPublisherService;
        private SessionRepository _sessionRepository;

        public BotAgentCommandController(SessionRepository sessionRepository, CommandPublisherService commandPublisherService)
        {
            _sessionRepository = sessionRepository;
            _commandPublisherService = commandPublisherService;
        }

        // GET: api/values
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        [HttpPost("{id}")]
        public async Task Post(string botname, string district, int id, [FromBody]BotAgentCommand command)
        {
            if (command == null || botname == null || district == null)
            {
                Response.StatusCode = 404;
                return;
            }


            var session = _sessionRepository.GetSession(botname, district, id);
            switch (command.Command)
            {
                case "switchcontrol":
                {
                    bool mcontrol = !session.ManualControl;
                    await _commandPublisherService.Publish(0, id, new List<IServiceCommand>()
                    {
                        new BotStateCommand()
                        {
                            MControl = mcontrol
                        }
                    });

                    break;
                }
                case "switchneedbotupdate":
                    {
                        bool switchneedbotupdate = !session.IsNeedUpdate;
                        await _commandPublisherService.Publish(0, id, new List<IServiceCommand>()
                    {
                        new BotStateCommand()
                        {
                            IsNeedUpdate = switchneedbotupdate
                        }
                    });

                        break;
                    }
            }
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
