﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BotMonitor.Repository;
using Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace BotMonitor.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    public class BotServiceController : Controller
    {
        private BotServiceRepository _botServiceRepository;

        public BotServiceController(BotServiceRepository botServiceRepository)
        {
            _botServiceRepository = botServiceRepository;
        }

        // GET: api/values
        [HttpGet]
        public ActionResult Get(string botname)
        {
            return Json(new BotCommand() {
                Services = _botServiceRepository.GetServices(botname),
                Name = botname
                             
                });
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
