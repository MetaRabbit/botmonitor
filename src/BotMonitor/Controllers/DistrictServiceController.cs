﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BotMonitor.Repository;
using BotMonitor.Service;
using Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace BotMonitor.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    public class DistrictServiceController : Controller
    {
        private DistrictServiceRepository _districtServiceRepository;
        private BotAgentTurnOffNotifyService _agentTurnOffNotifyService;

        public DistrictServiceController(DistrictServiceRepository districtServiceRepository, BotAgentTurnOffNotifyService agentTurnOffNotifyService)
        {
            _districtServiceRepository = districtServiceRepository;
            _agentTurnOffNotifyService = agentTurnOffNotifyService;
            _agentTurnOffNotifyService.Start();
        }

        // GET: api/values
        [HttpGet]
        public ActionResult Get(string botname, string district)
        {
            return Json(new BotCommand()
            {
                Services = _districtServiceRepository.GetServices(botname, district),
                Name = district

            });
        }


        // POST api/values
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
