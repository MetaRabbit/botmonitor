﻿using System;
using System.Collections.Generic;
using System.IO;
using BotMonitor.Extends;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace BotMonitor.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    public class PluginsMetadataController : Controller
    {
        // GET: api/values
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        [HttpGet("{namebot}")]
        public string Get(string namebot)
        {
            try
            {
                return System.IO.File.ReadAllText(Path.Combine("Resource", namebot, "Plugins", "version.json")); 
                     
            }
            catch (Exception)
            {
                Response.NotFoundRequest();
                return "";
            }
          
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
