﻿using BotMonitor.Extends;
using BotMonitor.Repository;
using Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace BotMonitor.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    public class SessionController : ControllerBase
    {
        SessionRepository _sessionRepository;

        // GET: api/values
        public SessionController(SessionRepository botAgentStateService)
        {
            _sessionRepository = botAgentStateService;
            //Заполнение фейковыми агентами
          
        }

        [HttpGet("{id}")]
        public ActionResult Get(string district, string botname, int id)
        {
           // if (!HttpContext.ConstrainsScope("botagentreadapi")) return new UnauthorizedResult();
            return new JsonResult(_sessionRepository.GetSession(botname,district, id));
        }

        [HttpGet]
        public ActionResult Get(string district, string botname)
        {
           // if (!HttpContext.ConstrainsScope("botagentreadapi")) return new UnauthorizedResult();
            return new JsonResult(_sessionRepository.GetSessions(botname, district));
        }

       

        // POST api/values
        [HttpPost("{botname}")]
        public ActionResult Post(string district, string botname, [FromBody]Session value)
        {
            if (!HttpContext.ConstrainsScope("botagentwriteapi")) return new UnauthorizedResult();
            if (value == null)
            {           
                return new BadRequestResult();
            }

            _sessionRepository.AddOrUpdateSession(botname, district, value);

            return new OkResult();
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
