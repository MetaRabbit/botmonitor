﻿using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using BotMonitor.Extends;
using BotMonitor.Service;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Net.Http.Headers;
using Newtonsoft.Json.Linq;

namespace BotMonitor.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    public class BotAgentController : ControllerBase
    {
        UpdateBotAgentService _botAgentService;
        private IHostingEnvironment _hostingEnvironment;
        public static string BaseAddress = "http://localhost:1941";

        public BotAgentController(UpdateBotAgentService botAgentService, IHostingEnvironment hostingEnvironment)
        {
            _botAgentService = botAgentService;
            _hostingEnvironment = hostingEnvironment;
        }

        // GET api/values/5
        [HttpGet]
        public ActionResult Get()
        {

            if (!HttpContext.ConstrainsScope("botagentreadapi")) return new UnauthorizedResult();

            if (!_botAgentService.ArchiveIsExists() || !_botAgentService.VersionFileIsExists())
            {
                return new NotFoundResult();
            }
                return new FileContentResult(_botAgentService.GetLastArchive(), new MediaTypeHeaderValue("application/zip"));
            
                //var client = new UserInfoClient(
                //  new Uri(BaseAddress + "/connect/userinfo"),
                //   HttpContext.Authentication.GetTokenAsync("access_token").Result);

                //var response = client.GetAsync().Result;

                //if (response.Claims.FirstOrDefault(claim => claim.Item1.Equals("role") && claim.Item2.Equals("BogAgentUpdater")) != null)
                //{ 
                //    if (!_botAgentService.ArchiveIsExists() || !_botAgentService.VersionFileIsExists())
                //{
                //    return new NotFoundResult();
                //}
                //    return new FileContentResult(_botAgentService.GetLastArchive(), new MediaTypeHeaderValue("application/zip"));
                //}

                //return new UnauthorizedResult();

        }

        // POST api/values 
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        [HttpPut]
        public async Task Put()
        {

            using (var fs = new FileStream(Path.Combine(_hostingEnvironment.ContentRootPath, "Resource", "botagent.zip"), FileMode.Create))
            {
                while (true)
                {
                    try
                    {

                        await Request.Body.CopyToAsync(fs);                  
                        break;
                    }
                    catch (Exception)
                    {
                        Thread.Sleep(400);
                        throw;
                    }
                }


            }
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
