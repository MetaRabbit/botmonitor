﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using BotMonitor.Extends;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Net.Http.Headers;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace BotMonitor.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    public class BotAgentSettingsController : Controller
    {
        private IHostingEnvironment _hostingEnvironment;

        public BotAgentSettingsController(IHostingEnvironment hostingEnvironment)
        {
            _hostingEnvironment = hostingEnvironment;
        }

        // GET: api/values
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        [HttpGet("{namebot}")]
        public ActionResult Get(string namebot)
        {
            try
            {
                var fileContentResult =
              new FileContentResult(
                  System.IO.File.ReadAllBytes(Path.Combine("Resource", namebot, "botagent", "settings.zip")),
                  new MediaTypeHeaderValue("application/zip"));
                fileContentResult.FileDownloadName = "settings";
                return fileContentResult; ;
            }
            catch (Exception)
            {
                return new BadRequestResult();
            }

          
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        [HttpPut]
        public async Task Put()
        {
            using (var fs = new FileStream(Path.Combine(_hostingEnvironment.ContentRootPath, "Resource", "version.txt"), FileMode.Create))
            {
                while (true)
                {
                    try
                    {

                        await Request.Body.CopyToAsync(fs);
                        break;
                    }
                    catch (Exception)
                    {
                        Thread.Sleep(400);
                        throw;
                    }
                }


            }

        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
