﻿using System;
using System.Linq;
using BotMonitor.Extends;
using BotMonitor.Service;
using IdentityModel.Client;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;


namespace BotMonitor.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    public class BotAgentMetadataController : ControllerBase
    {
        UpdateBotAgentService _botAgentService;
        public static string BaseAddress = "http://localhost:1941";

        public BotAgentMetadataController(UpdateBotAgentService botAgentService)
        {
            _botAgentService = botAgentService;
        }

        // GET api/values/5
        [HttpGet()]
        public ActionResult Get()
        {

            if (!HttpContext.ConstrainsScope("botagentreadapi")) return new UnauthorizedResult();

            if (!_botAgentService.VersionFileIsExists())
            {
                return new NotFoundResult();
            }
            string[] version = _botAgentService.GetHashLastArchive();
            return new JsonResult(new { Hash = version[0].Split(':')[1], ZipHash = version[1].Split(':')[1] });
        }

        [HttpGet("namebot")]
        public ActionResult Get(string namebot)
        {

            if (!HttpContext.ConstrainsScope("botagentreadapi")) return new UnauthorizedResult();

            if (!_botAgentService.VersionFileIsExists())
            {
                return new NotFoundResult();
            }
            string[] version = _botAgentService.GetHashLastArchive();
            return new JsonResult(new { Hash = version[0].Split(':')[1], ZipHash = version[1].Split(':')[1] });
        }
    }
}
