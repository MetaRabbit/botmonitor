﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Net.Http.Headers;
using Newtonsoft.Json.Linq;

namespace BotMonitor.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    public class PluginsController : Controller
    {
        private IHostingEnvironment _hostingEnvironment;

        public PluginsController(IHostingEnvironment hostingEnvironment)
        {
            _hostingEnvironment = hostingEnvironment;
        }

        // GET: api/values
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        [HttpGet("{namebot}")]
        public async Task<ActionResult> Get(string namebot)
        {
            try
            {
                byte[] body;
                using (var fs = new FileStream(Path.Combine(_hostingEnvironment.ContentRootPath,"Resource", namebot, "Plugins", "Plugins.zip"), FileMode.Open))
                {
                    body = new byte[fs.Length];
                    await fs.ReadAsync(body, 0, Convert.ToInt32(fs.Length));
                }
                var fileContentResult =
                    new FileContentResult(body, new MediaTypeHeaderValue("application/zip"))
                    {
                        FileDownloadName = "plugins"
                    };
                return fileContentResult; 
            }
            catch (Exception)
            {
                return new BadRequestResult();
            }
          
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{namebot}")]
        public async Task Put(string namebot)
        {
           
                
                using (var fs = new FileStream(Path.Combine(_hostingEnvironment.ContentRootPath, "Resource", namebot, "Plugins", "Plugins.zip"), FileMode.Create))
                 {
                     while (true)
                     {
                        try
                        {
                        
                            await Request.Body.CopyToAsync(fs);
                            var jVersion = JObject.Parse(System.IO.File.ReadAllText(Path.Combine("Resource", namebot, "Plugins", "version.json")));
                            jVersion["build"] = jVersion["build"].ToObject<long>() + 1;
                            System.IO.File.WriteAllText(Path.Combine("Resource", namebot, "Plugins", "version.json"), jVersion.ToString());
                            break;
                        }
                        catch (Exception)
                        {
                            Thread.Sleep(400);
                            throw;
                        }
                    }
                    
                     
                }               
            }
        

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
