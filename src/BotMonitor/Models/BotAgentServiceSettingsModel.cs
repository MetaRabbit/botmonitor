﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BotMonitor.Models
{
    public class BotAgentServiceSettingsModel
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }
}
