﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BotMonitor.Models
{
    public class BotAgentCommand
    {
        public string Command { get; set; }
        public string Args { get; set; }
        
    }
}
