﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BotMonitor.Models
{
    public class BotingGeneralStateInfo
    {
        public int NActive { get; set; }
        public int Nban { get; set; }
        public int NKeyExipred { get; set; }
        public int NKeysReserve { get; set; }
        public int NManualControl { get; set; }
        public bool IsBanWave { get; set; }
        public string BotName { get; set; }
        public int NDistrictUnavailable { get; set; }
    }
}
