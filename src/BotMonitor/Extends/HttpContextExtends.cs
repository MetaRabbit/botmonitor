﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IdentityModel;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json.Linq;

namespace BotMonitor.Extends
{
    public static class HttpContextExtends
    {
        public static bool ConstrainsScope(this HttpContext result, string api)
        {
            var parts = result.Authentication.GetTokenAsync("access_token").Result.Split('.');
            var claims = parts[1];

            var jObjectClaims = JObject.Parse(Encoding.UTF8.GetString(Base64Url.Decode(claims)));
            try
            {
                var scopes = jObjectClaims["scope"].Value<JArray>();

                if (scopes.FirstOrDefault(s => s.Value<string>().Equals(api)) != null)
                {
                    return true;
                }

               
            }
            catch (Exception)
            {
                var scopes = jObjectClaims["scope"].Value<string>();


                if (scopes.Equals(api))
                {
                    return true;
                }
            }
           
            return false;
        }
    }
}
