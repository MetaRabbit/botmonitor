﻿using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using BotMonitor.Infrastruction;
using BotMonitor.Repository;
using BotMonitor.Service;
using Data;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace BotMonitor
{
    public class Startup
    {

        public void ConfigureServices(IServiceCollection services)
        {

            services.AddMvc();
        
            services.AddSingleton<UpdateBotAgentService>();
            services.AddSingleton<ExileBuddyService>();
            services.AddSingleton<GameDataRepository>();
            services.AddSingleton<CommandPublisherService>();
            services.AddSingleton<DistrictRepository>();
            services.AddSingleton<SessionRepository>();

            services.AddSingleton<KeysService>(); 
            services.AddSingleton<BotServiceRepository>();
            services.AddSingleton<DistrictServiceRepository>();
            services.AddSingleton<BotAgentTurnOffNotifyService>();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear();

            app.UseIdentityServerAuthentication(new IdentityServerAuthenticationOptions
            {
                Authority = "http://localhost:1941",
                RequireHttpsMetadata = false,

                ScopeName = "gameinfoapi",
                AdditionalScopes = new List<string>() { "botagentwriteapi", "botagentreadapi" },
                AutomaticAuthenticate = true,
                AutomaticChallenge = true
           });

            loggerFactory.AddConsole();

            if (env.IsDevelopment())
            {
                loggerFactory.AddDebug();
                app.UseDeveloperExceptionPage();
            }
                  
            app.UseBrowserLink();
            app.UseMvc();
        }
    }
}
